# Installation of requirements necessary for exemple exec
requirements:
	pip3 install -r requirements.txt

dev-requirements:
	pip3 install -r requirements-dev.txt

# Build the installation file
installer: requirements dev-requirements
	pyinstaller js38sceptremaudit/main.py -n js38sceptremaudit --add-data "js38sceptremaudit/data/config/:data/config/" --add-data "js38sceptremaudit/data/:data/" --add-data "js38sceptremaudit/data/images/:data/images/"

# Clean installation (all)
clean:
	rm -rf build
	rm -rf dist
	rm -rf *.spec