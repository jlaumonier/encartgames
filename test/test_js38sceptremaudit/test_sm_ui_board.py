from pymasep.common import Action
from js38sceptremaudit import SMUIBoard
from js38sceptremaudit.game import SMGame


def test_calculate_state_square_for_moving_select_squares_first_with_none_success():
    validated_action = Action(environment=None, action_type=SMGame.ACTION_MOVE)
    validated_action.add_parameter('pos', [(0, 0)])
    desired_action = Action(environment=None, action_type=SMGame.ACTION_MOVE)
    desired_action.add_parameter('pos', [(0, 0)])
    selected_squares = []

    select, remove, errors = SMUIBoard.calculate_state_square_for_moving(validated_action,
                                                                         desired_action,
                                                                         selected_squares)

    # desired is selected
    assert select == [(0, 0)]
    assert remove == []
    assert errors == []


def test_calculate_state_square_for_moving_select_squares_first_with_none_error():
    validated_action = None
    desired_action = Action(environment=None, action_type=SMGame.ACTION_MOVE)
    desired_action.add_parameter('pos', [(2, 2)])
    selected_squares = []

    select, remove, errors = SMUIBoard.calculate_state_square_for_moving(validated_action,
                                                                         desired_action,
                                                                         selected_squares)

    assert select == []
    assert remove == []
    # desired is error
    assert errors == [(2, 2)]


def test_calculate_state_square_for_moving_select_squares_second_success():
    validated_action = Action(environment=None, action_type=SMGame.ACTION_MOVE)
    validated_action.add_parameter('pos', [(0, 0), (0, 1)])
    desired_action = Action(environment=None, action_type=SMGame.ACTION_MOVE)
    desired_action.add_parameter('pos', [(0, 1)])
    selected_squares = [(0, 0)]

    select, remove, errors = SMUIBoard.calculate_state_square_for_moving(validated_action,
                                                                         desired_action,
                                                                         selected_squares)

    # desired is selected
    assert select == [(0, 1)]
    assert remove == []
    assert errors == []


def test_calculate_state_square_for_moving_select_squares_second_error():
    validated_action = Action(environment=None, action_type=SMGame.ACTION_MOVE)
    validated_action.add_parameter('pos', [(0, 0)])
    desired_action = Action(environment=None, action_type=SMGame.ACTION_MOVE)
    desired_action.add_parameter('pos', [(2, 2)])
    selected_squares = [(0, 0)]

    select, remove, errors = SMUIBoard.calculate_state_square_for_moving(validated_action,
                                                                         desired_action,
                                                                         selected_squares)

    assert select == []
    assert remove == []
    # desired is error
    assert errors == [(2, 2)]


def test_calculate_state_square_for_moving_unselect_squares_first_success():
    validated_action = None
    desired_action = Action(environment=None, action_type=SMGame.ACTION_MOVE)
    desired_action.add_parameter('pos', [(0, 0)])
    selected_squares = [(0, 0)]

    select, remove, errors = SMUIBoard.calculate_state_square_for_moving(validated_action,
                                                                         desired_action,
                                                                         selected_squares)

    assert select == []
    # desired is removed
    assert remove == [(0, 0)]
    assert errors == []


def test_calculate_state_square_for_moving_unselect_squares_second_success():
    validated_action = Action(environment=None, action_type=SMGame.ACTION_MOVE)
    validated_action.add_parameter('pos', [(0, 0)])
    desired_action = Action(environment=None, action_type=SMGame.ACTION_MOVE)
    desired_action.add_parameter('pos', [(0, 1)])
    selected_squares = [(0, 0), (0, 1)]

    select, remove, errors = SMUIBoard.calculate_state_square_for_moving(validated_action,
                                                                         desired_action,
                                                                         selected_squares)

    assert select == []
    # desired is removed
    assert remove == [(0, 1)]
    assert errors == []


def test_calculate_state_square_for_moving_unselect_squares_first_with_path_success():
    validated_action = None
    desired_action = Action(environment=None, action_type=SMGame.ACTION_MOVE)
    desired_action.add_parameter('pos', [(0, 0)])
    selected_squares = [(0, 0), (0, 1), (0, 2), (0, 3)]

    select, remove, errors = SMUIBoard.calculate_state_square_for_moving(validated_action,
                                                                         desired_action,
                                                                         selected_squares)

    assert select == []
    # desired is removed
    assert remove == [(0, 0), (0, 1), (0, 2), (0, 3)]
    assert errors == []


def test_calculate_state_square_for_moving_other_action():
    validated_action = Action(environment=None, action_type=SMGame.ACTION_OPEN)
    validated_action.add_parameter('door_name', 'Door1')
    desired_action = Action(environment=None, action_type=SMGame.ACTION_OPEN)
    desired_action.add_parameter('door_name', 'Door1')
    selected_squares = [(0, 0), (0, 1), (0, 2), (0, 3)]

    select, remove, errors = SMUIBoard.calculate_state_square_for_moving(validated_action,
                                                                         desired_action,
                                                                         selected_squares)

    assert select == []
    # desired is removed
    assert remove == [(0, 0), (0, 1), (0, 2), (0, 3)]
    assert errors == []


def test_calculate_state_square_for_moving_unselect_squares_second_with_path_success():
    validated_action = Action(environment=None, action_type=SMGame.ACTION_MOVE)
    validated_action.add_parameter('pos', [(0, 0)])
    desired_action = Action(environment=None, action_type=SMGame.ACTION_MOVE)
    desired_action.add_parameter('pos', [(0, 1)])
    selected_squares = [(0, 0), (0, 1), (0, 2), (0, 3)]

    select, remove, errors = SMUIBoard.calculate_state_square_for_moving(validated_action,
                                                                         desired_action,
                                                                         selected_squares)

    assert select == []
    # desired is removed
    assert remove == [(0, 1), (0, 2), (0, 3)]
    assert errors == []


def test_calculate_state_square_for_moving_select_squares_no_actions():
    validated_action = None
    desired_action = None
    selected_squares = []

    select, remove, errors = SMUIBoard.calculate_state_square_for_moving(validated_action,
                                                                         desired_action,
                                                                         selected_squares)

    # desired is selected
    assert select == []
    assert remove == []
    assert errors == []


def test_calculate_state_square_for_moving_unselect_squares_no_actions():
    validated_action = None
    desired_action = None
    selected_squares = [(0, 0), (0, 1)]

    select, remove, errors = SMUIBoard.calculate_state_square_for_moving(validated_action,
                                                                         desired_action,
                                                                         selected_squares)

    # desired is selected
    assert select == []
    assert remove == [(0, 0), (0, 1)]
    assert errors == []


def test_calculate_state_door_for_opening_door_cannot_be_opened():
    validated_action = None
    desired_action = Action(environment=None, action_type=SMGame.ACTION_OPEN)
    desired_action.add_parameter('door_name', 'Door1')
    selected_door = None

    select, remove, errors = SMUIBoard.calculate_state_door_for_opening(validated_action,
                                                                        desired_action,
                                                                        selected_door)

    # desired is not selected
    assert select == []
    assert remove == []
    assert errors == ['Door1']


def test_calculate_state_door_for_opening_door_can_be_opened():
    validated_action = Action(environment=None, action_type=SMGame.ACTION_OPEN)
    validated_action.add_parameter('door_name', 'Door1')
    desired_action = Action(environment=None, action_type=SMGame.ACTION_OPEN)
    desired_action.add_parameter('door_name', 'Door1')
    selected_door = None

    select, remove, errors = SMUIBoard.calculate_state_door_for_opening(validated_action,
                                                                        desired_action,
                                                                        selected_door)

    # desired is not validated
    assert select == ['Door1']
    assert remove == []
    assert errors == []


def test_calculate_state_door_for_opening_door_unselect_door():
    validated_action = None
    desired_action = Action(environment=None, action_type=SMGame.ACTION_OPEN)
    desired_action.add_parameter('door_name', 'Door1')
    selected_door = 'Door1'

    select, remove, errors = SMUIBoard.calculate_state_door_for_opening(validated_action,
                                                                        desired_action,
                                                                        selected_door)

    # desired is removed
    assert select == []
    assert remove == ['Door1']
    assert errors == []


def test_calculate_state_door_for_opening_other_action():
    validated_action = None
    desired_action = Action(environment=None, action_type=SMGame.ACTION_MOVE)
    selected_door = 'Door1'

    select, remove, errors = SMUIBoard.calculate_state_door_for_opening(validated_action,
                                                                        desired_action,
                                                                        selected_door)
    # selected is removed
    assert select == []
    assert remove == ['Door1']
    assert errors == []


def test_calculate_state_door_for_opening_other_action_none_selected():
    validated_action = None
    desired_action = Action(environment=None, action_type=SMGame.ACTION_MOVE)
    selected_door = None

    select, remove, errors = SMUIBoard.calculate_state_door_for_opening(validated_action,
                                                                        desired_action,
                                                                        selected_door)
    # nothing change
    assert select == []
    assert remove == []
    assert errors == []


def test_calculate_state_door_for_opening_unselect_door_no_actions():
    validated_action = None
    desired_action = None
    selected_door = 'Door1'

    select, remove, errors = SMUIBoard.calculate_state_door_for_opening(validated_action,
                                                                        desired_action,
                                                                        selected_door)
    # remove all
    assert select == []
    assert remove == ['Door1']
    assert errors == []

def test_calculate_state_door_for_opening_choose_other_door():
    validated_action = Action(environment=None, action_type=SMGame.ACTION_OPEN)
    validated_action.add_parameter('door_name', 'Door1')
    desired_action = Action(environment=None, action_type=SMGame.ACTION_OPEN)
    desired_action.add_parameter('door_name', 'Door2')
    selected_door = 'Door1'

    select, remove, errors = SMUIBoard.calculate_state_door_for_opening(validated_action,
                                                                        desired_action,
                                                                        selected_door)
    # error the desired door, other door is selected
    assert select == []
    assert remove == []
    assert errors == ['Door2']