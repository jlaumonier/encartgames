import pytest

from js38sceptremaudit.game import DoorRules


def test_open_success_broken():
    strength = 8
    door_state = DoorRules.STATUS_CLOSED

    result_door_state, result_lost_energy, result_additional_info = DoorRules.open(door_state,
                                                                                   strength,
                                                                                   random_seed=1)

    assert result_door_state == DoorRules.STATUS_BROKEN
    assert result_lost_energy == 0
    assert result_additional_info == ['Résultat du dé : 7', 'Porte enfoncée']


def test_open_fail():
    strength = 6
    door_state = DoorRules.STATUS_CLOSED

    result_door_state, result_lost_energy, result_additional_info = DoorRules.open(door_state,
                                                                                   strength,
                                                                                   random_seed=1)

    assert result_door_state == DoorRules.STATUS_CLOSED
    assert result_lost_energy == 2
    assert result_additional_info == ['Résultat du dé : 7', "Échec de l'ouverture de la porte",
                                      "Perte de 2 points d'énergie"]
