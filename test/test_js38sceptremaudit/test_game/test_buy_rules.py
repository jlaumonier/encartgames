import pytest

from js38sceptremaudit.game import BuyRules


def test_buy_rules_add_buy_intention_from_zero():
    object_agent_intention = []
    object_intention_to_buy = ['object1']
    object_intention_not_to_buy = []

    result, remaining_ressource, costs = BuyRules.buy_rules(object_agent_intention, object_intention_to_buy,
                                                            object_intention_not_to_buy)

    assert result == ['object1']
    assert remaining_ressource is None
    assert costs is None


def test_buy_rules_add_buy_intention_from_existing():
    object_agent_intention = ['object1']
    object_intention_to_buy = ['object2']
    object_intention_not_to_buy = []

    result, remaining_ressource, costs = BuyRules.buy_rules(object_agent_intention, object_intention_to_buy,
                                                            object_intention_not_to_buy)

    assert result == ['object1', 'object2']
    assert remaining_ressource is None
    assert costs is None


def test_buy_rules_remove_buy_intention_from_existing():
    object_agent_intention = ['object1']
    object_intention_to_buy = []
    object_intention_not_to_buy = ['object1']

    result, remaining_ressource, costs = BuyRules.buy_rules(object_agent_intention, object_intention_to_buy,
                                                            object_intention_not_to_buy)

    assert result == []
    assert remaining_ressource is None
    assert costs is None


def test_buy_rules_add_with_ressources():
    object_agent_intention = []
    object_intention_to_buy = ['object1']
    object_intention_not_to_buy = []
    objects_costs = {"object1": 1, "object2": 2}
    available_ressource = 1

    result, remaining_ressource, costs = BuyRules.buy_rules(object_agent_intention,
                                                            object_intention_to_buy,
                                                            object_intention_not_to_buy,
                                                            available_ressource,
                                                            objects_costs)

    assert result == ['object1']
    assert remaining_ressource == 0
    assert costs == 1


def test_buy_rules_add_with_ressources_not_enough():
    object_agent_intention = []
    object_intention_to_buy = ['object2']
    object_intention_not_to_buy = []
    objects_costs = {"object1": 1, "object2": 2}
    available_ressource = 1

    result, remaining_ressource, costs = BuyRules.buy_rules(object_agent_intention,
                                                            object_intention_to_buy,
                                                            object_intention_not_to_buy,
                                                            available_ressource,
                                                            objects_costs)

    assert result == []
    assert remaining_ressource == 1
    assert costs == 0


def test_buy_rules_remove_with_ressources():
    object_agent_intention = ['object1']
    object_intention_to_buy = []
    object_intention_not_to_buy = ['object1']
    objects_costs = {"object1": 1, "object2": 2}
    available_ressource = 1

    result, remaining_ressource, costs = BuyRules.buy_rules(object_agent_intention,
                                                            object_intention_to_buy,
                                                            object_intention_not_to_buy,
                                                            available_ressource,
                                                            objects_costs)

    assert result == []
    assert remaining_ressource == 2
    assert costs == 0


def test_buy_rules_no_intention():
    object_agent_intention = ['object1']
    object_intention_to_buy = []
    object_intention_not_to_buy = []
    objects_costs = {"object1": 1, "object2": 2}
    available_ressource = 1

    result, remaining_ressource, costs = BuyRules.buy_rules(object_agent_intention,
                                                            object_intention_to_buy,
                                                            object_intention_not_to_buy,
                                                            available_ressource,
                                                            objects_costs)

    assert result == ['object1']
    assert remaining_ressource == 1
    assert costs == 1


def test_buy_rules_buy_and_not_buy():
    object_agent_intention = ['object1', 'object3']
    object_intention_to_buy = ['object2']
    object_intention_not_to_buy = ['object3']
    objects_costs = {"object1": 1, "object2": 2, "object3": 4}
    available_ressource = 1

    result, remaining_ressource, costs = BuyRules.buy_rules(object_agent_intention,
                                                            object_intention_to_buy,
                                                            object_intention_not_to_buy,
                                                            available_ressource,
                                                            objects_costs)

    assert result == ['object1', 'object2']
    assert remaining_ressource == 3
    assert costs == 3
