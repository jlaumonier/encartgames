import pytest
from xml.etree.ElementTree import fromstring

from pymasep.common.math import GridTools
from js38sceptremaudit.game import MovingRules


def test_move_character_one_step():
    positions = [(0, 1)]

    result_dest, lost_energy, result_additional_info = MovingRules.move(positions)

    assert result_dest == (0, 1)
    assert lost_energy == 0
    assert isinstance(lost_energy, int)
    assert result_additional_info == []


def test_move_character_two_step_success():
    positions = [(0, 1), (0, 2)]

    result_dest, lost_energy, result_additional_info = MovingRules.move(positions, random_seed=0)

    assert result_dest == (0, 2)
    assert lost_energy == 0
    assert result_additional_info == ['Résultat du dé : 4', 'Pas de chute', "Pas de perte d'énergie"]


def test_move_character_two_step_failure_1st_pos():
    positions = [(0, 1), (0, 2)]

    result_dest, lost_energy, result_additional_info = MovingRules.move(positions, random_seed=2)

    assert result_dest == (0, 1)
    assert lost_energy == 1
    assert result_additional_info == ['Résultat du dé : 1',
                                      'Chute sur la 1ere case',
                                      "Perte de 1 point(s) d'énergie"]


def test_move_character_two_step_failure_2nd_pos():
    positions = [(0, 1), (0, 2)]

    result_dest, lost_energy, result_additional_info = MovingRules.move(positions, random_seed=3)

    assert result_dest == (0, 2)
    assert lost_energy == 2
    assert result_additional_info == ['Résultat du dé : 2',
                                      'Chute sur la 2e case',
                                      "Perte de 2 point(s) d'énergie"]


def test_update_graph_with_door_status_door_closed(mock_environment):
    data_map = [[0x9, 0x1, 0x1, 0x5, 0xA, 0x2],
                [0x8, 0x0, 0x0, 0x0, 0x3, 0x3],
                [0x8, 0x0, 0x0, 0x4, 0x9, 0x1],
                [0xA, 0x2, 0x2, 0x4, 0x8, 0x2]]

    graph = GridTools.create_graph(data_map)

    door_objects_xml_str = '<Object name="Door1" nature="Door">' \
                           '<ObjectState name="state">' \
                           '<Characteristic name="Pos1">' \
                           '<tuple><item><int>1</int></item><item><int>1</int></item></tuple>' \
                           '</Characteristic>' \
                           '<Characteristic name="Pos2">' \
                           '<tuple><item><int>2</int></item><item><int>1</int></item></tuple>' \
                           '</Characteristic>' \
                           '<Characteristic name="status">' \
                           '<str>closed</str>' \
                           '</Characteristic>' \
                           '</ObjectState>' \
                           '</Object>'

    xml_node = fromstring(door_objects_xml_str)
    door_object = mock_environment.create_object('Door1', xml_node=xml_node)

    assert graph.has_edge((1, 1), (2, 1))
    assert graph.has_edge((2, 1), (1, 1))

    new_graph = MovingRules.update_graph_with_door_status(graph, [door_object])

    assert not new_graph.has_edge((1, 1), (2, 1))
    assert not new_graph.has_edge((2, 1), (1, 1))


def test_update_graph_with_tokens_no_moving_agent(mock_environment):
    data_map = [[0x9, 0x1, 0x1, 0x5, 0xA, 0x2],
                [0x8, 0x0, 0x0, 0x0, 0x3, 0x3],
                [0x8, 0x0, 0x0, 0x0, 0x3, 0x3],
                [0x8, 0x0, 0x0, 0x4, 0x9, 0x1],
                [0xA, 0x2, 0x2, 0x4, 0x8, 0x2]]

    graph = GridTools.create_graph(data_map)

    character_xml_str = '<Agent name="player0" controller="js38sceptremaudit.sm_player_controller.SMPlayerController">' \
                        '<ObjectState name="state">' \
                        '<Characteristic name="Pos">' \
                        '<tuple><item><int>2</int></item><item><int>2</int></item></tuple>' \
                        '</Characteristic>' \
                        '</ObjectState>' \
                        '</Agent>'

    xml_node = fromstring(character_xml_str)
    player_object = mock_environment.create_object('player0', xml_node=xml_node)

    assert graph.has_edge((2, 2), (3, 2))
    assert graph.has_edge((2, 2), (1, 2))
    assert graph.has_edge((2, 2), (2, 1))
    assert graph.has_edge((2, 2), (1, 2))
    assert graph.has_edge((3, 2), (2, 2))
    assert graph.has_edge((1, 2), (2, 2))
    assert graph.has_edge((2, 1), (2, 2))
    assert graph.has_edge((1, 2), (2, 2))

    new_graph = MovingRules.update_graph_with_tokens(graph, [player_object], None)

    assert not new_graph.has_edge((2, 2), (3, 2))
    assert not new_graph.has_edge((2, 2), (1, 2))
    assert not new_graph.has_edge((2, 2), (2, 1))
    assert not new_graph.has_edge((2, 2), (1, 2))
    assert not new_graph.has_edge((3, 2), (2, 2))
    assert not new_graph.has_edge((1, 2), (2, 2))
    assert not new_graph.has_edge((2, 1), (2, 2))
    assert not new_graph.has_edge((1, 2), (2, 2))


def test_update_graph_with_tokens_moving_agent(mock_environment):
    data_map = [[0x9, 0x1, 0x1, 0x5, 0xA, 0x2],
                [0x8, 0x0, 0x0, 0x0, 0x3, 0x3],
                [0x8, 0x0, 0x0, 0x0, 0x3, 0x3],
                [0x8, 0x0, 0x0, 0x4, 0x9, 0x1],
                [0xA, 0x2, 0x2, 0x4, 0x8, 0x2]]

    graph = GridTools.create_graph(data_map)

    character_xml_str = '<Agent name="player0" controller="js38sceptremaudit.sm_player_controller.SMPlayerController">' \
                        '<ObjectState name="state">' \
                        '<Characteristic name="Pos">' \
                        '<tuple><item><int>2</int></item><item><int>2</int></item></tuple>' \
                        '</Characteristic>' \
                        '</ObjectState>' \
                        '</Agent>'

    xml_node = fromstring(character_xml_str)
    player_object = mock_environment.create_object('player0', xml_node=xml_node)

    assert graph.has_edge((2, 2), (3, 2))
    assert graph.has_edge((2, 2), (1, 2))
    assert graph.has_edge((2, 2), (2, 1))
    assert graph.has_edge((2, 2), (1, 2))
    assert graph.has_edge((3, 2), (2, 2))
    assert graph.has_edge((1, 2), (2, 2))
    assert graph.has_edge((2, 1), (2, 2))
    assert graph.has_edge((1, 2), (2, 2))

    new_graph = MovingRules.update_graph_with_tokens(graph, [player_object], player_object)

    assert new_graph.has_edge((2, 2), (3, 2))
    assert new_graph.has_edge((2, 2), (1, 2))
    assert new_graph.has_edge((2, 2), (2, 1))
    assert new_graph.has_edge((2, 2), (1, 2))
    assert new_graph.has_edge((3, 2), (2, 2))
    assert new_graph.has_edge((1, 2), (2, 2))
    assert new_graph.has_edge((2, 1), (2, 2))
    assert new_graph.has_edge((1, 2), (2, 2))
