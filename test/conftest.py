# content of conftest.py
import pytest

from js38sceptremaudit.game import SMGame

pytest_plugins = ["pymasep.tests.fixtures"]

@pytest.fixture(scope="function")
def game_class():
    """
    Create a game according to the current project
    """
    yield SMGame

