from pygame.event import custom_type


""" Player just choose an action """
SM_ACTION_CHOICE = custom_type()
""" A door has been selected """
SM_ACTION_DOOR_SELECTED = custom_type()
""" a selected door has been validated """
SM_ACTION_DOOR_VALIDATED = custom_type()
