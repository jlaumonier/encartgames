from pygame_gui.elements import UIWindow
from pygame_gui.elements import UILabel
from pygame import Rect


class SMSheet(UIWindow):
    """
    Player Sheet that display player informations

    :param rect: size ond position of the sheet
    :param manager: Manager (from pygame_gui)
    :param sub_application: Interface sub application
    :param agent_name: Name of the agent handled by this sheet
    """

    def __init__(self, rect, manager, sub_application, agent_name):
        super().__init__(rect, manager,
                         window_display_title='Feuille de personnage',
                         object_id='#sheet',
                         resizable=False
                         )
        self.sub_application = sub_application
        self.agent_name = agent_name
        font_info = self.ui_manager.get_theme().get_font_info(self.object_ids)
        margin = int(font_info['size']) / 2

        self.name_label = UILabel(Rect((margin, margin),
                                       (self.get_relative_rect()[0], int(int(font_info['size']) * 1.5))),
                                  text='Nom : ',
                                  manager=self.ui_manager,
                                  container=self,
                                  object_id='#sheet')
        self.energy_label = UILabel(
            Rect((margin, self.name_label.get_relative_rect()[1] + int(int(font_info['size']) * 1.5)),
                 (self.get_relative_rect()[0], int(int(font_info['size']) * 1.5))),
            text='Energie : ',
            manager=self.ui_manager,
            container=self,
            object_id='#sheet')
        self.strength_label = UILabel(
            Rect((margin, self.energy_label.get_relative_rect()[1] + int(int(font_info['size']) * 1.5)),
                 (self.get_relative_rect()[0], int(int(font_info['size']) * 1.5))),
            text='Force : ',
            manager=self.ui_manager,
            container=self,
            object_id='#sheet')
        self.po_label = UILabel(
            Rect((margin, self.strength_label.get_relative_rect()[1] + int(int(font_info['size']) * 1.5)),
                 (self.get_relative_rect()[0], int(int(font_info['size']) * 1.5))),
            text="Pièces d'or : ",
            manager=self.ui_manager,
            container=self,
            object_id='#sheet')
        self.weapon_label = UILabel(
            Rect((margin, self.po_label.get_relative_rect()[1] + int(int(font_info['size']) * 1.5)),
                 (self.get_relative_rect()[0], int(int(font_info['size']) * 1.5))),
            text='Arme : ',
            manager=self.ui_manager,
            container=self,
            object_id='#sheet')
        self.change_layer(50)

    def update(self, time_delta: float):
        """
        Update information from agent state

        :param time_delta: time passed since the last call
        """
        super().update(time_delta)
        cs = self.sub_application.environment.current_state
        os = cs.objects[self.agent_name].object_state
        wpn_container = cs.objects[self.agent_name].containers['Armes']
        self.name_label.set_text('Nom : ' + str(os.characteristics['Nom'].value) + '(' + self.agent_name + ')')
        self.energy_label.set_text('Energie : ' + str(os.characteristics['Energie'].value))
        self.strength_label.set_text('Force : ' + str(os.characteristics['Force'].value))
        self.po_label.set_text("Pièces d'or : " + str(os.characteristics['PO'].value))
        if len(wpn_container) > 0:
            wpn_label = 'Armes : '
            for w in wpn_container:
                weapon_str = w.object_state.characteristics['Nom'].value
                dommage_str = w.object_state.characteristics['dommages'].value
                wpn_label = wpn_label + weapon_str + ' (' + dommage_str + '), '
            self.weapon_label.set_text(wpn_label)
