import pygame.draw
import pymasep.common
from pygame.event import Event
from pygame import Rect, Surface, MOUSEMOTION, MOUSEBUTTONDOWN

from pygame_gui.core.interfaces import IUIManagerInterface, IContainerLikeInterface
from pygame_gui.elements import UIImage

from pymasep.application import SubApp
from pymasep.interface.ui_board import UIBoard, UIBoardMode, UIBoardSquare, UIBoardToken
from pymasep.interface.user_event import SQUARE_SELECTED_EVENT, SQUARE_VALIDATION_EVENT
from pymasep.common.math import GridTools

from js38sceptremaudit.game import SMGame
from js38sceptremaudit.graphics_data import step_upleft
from js38sceptremaudit.sm_user_event import SM_ACTION_DOOR_SELECTED, SM_ACTION_DOOR_VALIDATED


class SMUIBoardModeSelectDoor(UIBoardMode):
    """
    UIBoard select door mode. Select a door closed to the agent

    :param board: the UIBoard where this mode is defined
    """

    def process_event(self, event: Event) -> bool:
        """
        process the event received by the UIBoard: Hover door on MOUSEMOTION and select/deselect on MOUSEBUTTONDOWN

        :param event: event to process
        :return: true if the event has been processed
        """
        event_processed = False
        if event.type == MOUSEMOTION:
            for door_token in self.board.door_tokens.values():
                if door_token.hovered:
                    door_token.hovered_for_action = True
                else:
                    door_token.hovered_for_action = False
                event_processed = True
        if event.type == MOUSEBUTTONDOWN:
            if event.button == 1:
                door_hovered_name = self.board.get_door_token_name_hovered()
                if door_hovered_name:
                    evt_door_selected = pygame.event.Event(SM_ACTION_DOOR_SELECTED,
                                                           door_name=door_hovered_name)
                    pygame.event.post(evt_door_selected)
                    event_processed = True
            if event.button == 3:
                door_selected_name = self.board.get_door_token_name_from_state(UIBoardToken.STATE_SELECTED)
                if door_selected_name:
                    evt_door_validation = pygame.event.Event(SM_ACTION_DOOR_VALIDATED,
                                                             door_name=door_selected_name)
                    pygame.event.post(evt_door_validation)
                    event_processed = True
        return event_processed

    def init(self) -> None:
        """ Init the mode. Nothing in this case """
        pass

    def clean(self) -> None:
        """ Clean the mode : unhover door """
        self.board.door_name_hovered = None


class SMUIBoardDoor(UIBoardToken):
    """
    Door token that can be put on the board. Position is

    :param manager: Manager (from pygame_gui)
    :param board: Board where this element is put
    :param square_size: square size (in pixel)
    :param door_pos: position of a door (2 coordinates, a door is between 2 squares)
    :param image_surface: image surface of the element to be displayed
    :param status: status of the door (Open, closed, broken, see DoorRules)
    """

    def __init__(self, manager, board, square_size, door_pos, image_surface, status):
        position_door = ((door_pos[0][0] + door_pos[1][0]) / 2,
                         (door_pos[0][1] + door_pos[1][1]) / 2)
        self.status = status
        super().__init__(manager=manager,
                         board=board,
                         position=position_door,
                         square_size=square_size,
                         image_surface=image_surface)


class SMUIBoard(UIBoard):
    """
    Board game for Sceptre Maudit

    :param relative_rect: position and dimensions of the board (pixels)
    :param manager: Manager (from pygame_gui)
    :param container: Container of the board (form pygame_gui)
    :param sub_application: sub application instance (the interface)
    :param image_surface: the image surface to display the board
    :param game_data: the data map (see @GridTools)
    :param game_class_: the class of the game
    :param origin_coord_map: pixel coordinate of the origin of the (0, 0) map position
    :param square_size: size of a square of the board
    :param player_name: name of the player handle by the board

    For information, layer of elements:
      Door : 95
    """

    """ Mode of the board : Selection of the door"""
    SELECT_DOOR_MODE = 'SelectDoor'

    def __init__(self, relative_rect: Rect,
                 manager: IUIManagerInterface,
                 container: IContainerLikeInterface,
                 sub_application: SubApp,
                 image_surface: Surface,
                 game_data,
                 game_class_,
                 origin_coord_map,
                 square_size,
                 player_name
                 ):
        super().__init__(relative_rect, manager, container, sub_application, image_surface, game_data,
                         game_class_, origin_coord_map, square_size, player_name)
        self.modes[SMUIBoard.SELECT_DOOR_MODE] = SMUIBoardModeSelectDoor(self)
        step_rect = Rect((0, 0), self.sub_application.resources['pawn_step'].get_size())
        self.pawn_step = UIImage(relative_rect=step_rect,
                                 image_surface=self.sub_application.resources['pawn_step'],
                                 manager=manager,
                                 container=self)
        self.add_element(self.pawn_step)
        self.set_mode(UIBoard.SELECT_SQUARE_MODE)
        self.door_tokens = {}

    def process_event(self, event: Event) -> bool:
        """
        Process an event on the board.

        :param event: the event to process
        :return: True if it has been processed
        """
        event_processed = super().process_event(event)
        if event.type in [SQUARE_SELECTED_EVENT, SQUARE_VALIDATION_EVENT, SM_ACTION_DOOR_SELECTED,
                          SM_ACTION_DOOR_VALIDATED]:
            event_processed = self.sub_application.environment.controlled_agent.controller.select_action(
                event) or event_processed
        return event_processed

    def _update_debug(self):
        # map data debug
        for y, line in enumerate(self.game_data):
            for x, cell in enumerate(line):
                if cell & GridTools.MOVE_NORTH_BLOCKED:
                    pygame.draw.line(self.image, pygame.Color(255, 0, 0),
                                     (int(self.origin_coord_map[0] + (x * self.square_size)),
                                      self.origin_coord_map[1] + (y * self.square_size)),
                                     (int(self.origin_coord_map[0] + ((x + 1) * self.square_size)),
                                      self.origin_coord_map[1] + (y * self.square_size)),
                                     width=5)
                if cell & GridTools.MOVE_SOUTH_BLOCKED:
                    pygame.draw.line(self.image, pygame.Color(0, 255, 0),
                                     (int(self.origin_coord_map[0] + (x * self.square_size)),
                                      self.origin_coord_map[1] + ((y + 1) * self.square_size) - 5),
                                     (int(self.origin_coord_map[0] + ((x + 1) * self.square_size)),
                                      self.origin_coord_map[1] + ((y + 1) * self.square_size) - 5),
                                     width=5)
                if cell & GridTools.MOVE_EAST_BLOCKED:
                    pygame.draw.line(self.image, pygame.Color(0, 0, 255),
                                     (int(self.origin_coord_map[0] + ((x + 1) * self.square_size) - 5),
                                      self.origin_coord_map[1] + (y * self.square_size)),
                                     (int(self.origin_coord_map[0] + ((x + 1) * self.square_size) - 5),
                                      self.origin_coord_map[1] + ((y + 1) * self.square_size)),
                                     width=5)
                if cell & GridTools.MOVE_WEST_BLOCKED:
                    pygame.draw.line(self.image, pygame.Color(255, 0, 255),
                                     (int(self.origin_coord_map[0] + (x * self.square_size)),
                                      self.origin_coord_map[1] + (y * self.square_size)),
                                     (int(self.origin_coord_map[0] + (x * self.square_size)),
                                      self.origin_coord_map[1] + ((y + 1) * self.square_size)),
                                     width=5)

    @staticmethod
    def calculate_state_square_for_moving(validated_action, desired_action, selected_squares):
        """
        calculate which square should be selected, removed and display as error after a move (or intention of moving)

        :param validated_action: move action (intention) validated by the game (you just lost !)
        :param desired_action: move action desired by the agent (before validation)
        :param selected_squares: board squares already selected
        :return: squares to select, squares to remove, squares to display as error
        """
        select = []
        remove = []
        error = []
        validated_pos = []
        desired_pos = []
        if validated_action is not None and validated_action.type == SMGame.ACTION_MOVE:
            validated_pos = validated_action.params['pos']
        if desired_action is not None and desired_action.type == SMGame.ACTION_MOVE:
            desired_pos = desired_action.params['pos']
        if desired_pos and desired_pos[0] in validated_pos:
            select = desired_pos
        if desired_pos and (desired_pos[0] in selected_squares and desired_pos[0] not in validated_pos):
            for p in selected_squares:
                if p not in validated_pos:
                    remove.append(p)
        if not desired_pos and not validated_pos:
            remove = selected_squares
        if desired_pos and (desired_pos[0] not in selected_squares and desired_pos[0] not in validated_pos):
            error = desired_pos
        return select, remove, error

    @staticmethod
    def calculate_state_door_for_opening(validated_action, desired_action, selected_door):
        """
        Calculate if a door should be selected, selection removed or display as error after a open action (or intention)

        :param validated_action: open action (intention) validated by the game.
        :param desired_action: open action desired by the agent (before validation)
        :param selected_door: door currently selected (may be empty)
        :return: door to select, selection to removed and displayed as error
        """
        select = []
        remove = []
        error = []
        if validated_action and validated_action.type == SMGame.ACTION_OPEN:
            if not selected_door:
                select.append(validated_action.params['door_name'])
            else:
                if desired_action and desired_action.type == SMGame.ACTION_OPEN:
                    desired_door = desired_action.params['door_name']
                    error.append(desired_door)
        else:
            if desired_action and desired_action.type == SMGame.ACTION_OPEN:
                desired_door = desired_action.params['door_name']
                if selected_door and desired_door in selected_door:
                    remove.append(desired_door)
                else:
                    error.append(desired_door)
            else:
                if selected_door is not None:
                    remove.append(selected_door)
        return select, remove, error

    def get_door_token_name_hovered(self):
        """
        Get of the first hovered door name
        :return: hovered door name
        """
        result = None
        result_list = None
        if self.door_tokens:
            result_list = [d for d in self.door_tokens.keys() if self.door_tokens[d].hovered]
        if result_list:
            result = result_list[0]
        return result

    def get_door_token_name_from_state(self, state):
        """
        Get th first door names according to a state (selected, error)
        :param state: the state to find
        :return: the first door name with state
        """
        result = None
        if self.door_tokens:
            result_list = [d for d in self.door_tokens.keys() if self.door_tokens[d].state == state]
            if result_list:
                result = result_list[0]
        return result

    def _update_doors_with_observation(self,
                                       validated_action: pymasep.common.Action,
                                       desired_action: pymasep.common.Action,
                                       state: pymasep.common.State
                                       ):
        selected_door = self.get_door_token_name_from_state(SMUIBoardDoor.STATE_SELECTED)
        select_door, remove_door, errors_door = SMUIBoard.calculate_state_door_for_opening(validated_action,
                                                                                           desired_action,
                                                                                           selected_door)
        for d in select_door:
            self.door_tokens[d].state = SMUIBoardDoor.STATE_SELECTED
        for d in remove_door:
            self.door_tokens[d].state = None
        for d in errors_door:
            self.door_tokens[d].state = SMUIBoardDoor.STATE_ERROR

        for door in [state.objects[o] for o in state.objects if state.objects[o].nature == 'Door']:
            door_status = door.object_state.characteristics['status'].value
            door_surface = self.sub_application.resources[self.sub_application.DOOR_TOKENS[door_status]]
            positions_door = [door.object_state.characteristics['Pos1'].value,
                              door.object_state.characteristics['Pos2'].value]
            if door.get_fullname() not in self.door_tokens.keys() \
                    or self.door_tokens[door.get_fullname()].status != door_status:
                self.door_tokens[door.get_fullname()] = SMUIBoardDoor(manager=self.ui_manager,
                                                                      board=self,
                                                                      door_pos=positions_door,
                                                                      square_size=self.square_size,
                                                                      image_surface=door_surface,
                                                                      status=door_status)
                self.door_tokens[door.get_fullname()].change_layer(95)

    def update_with_observation(self, time_delta: float):
        """
        Part of the update directly depending on the observation and that must be called only once per observation

        :param time_delta: time passed since the last call
        """
        super().update_with_observation(time_delta)

        state = self.sub_application.environment.current_state

        validated_action = None
        desired_action = None
        player_intention = state.objects[self.player_name].intention
        self.sub_application.logger.debug('Player :' + str(self.player_name))
        self.sub_application.logger.debug('Intention :' + str(player_intention))
        if player_intention is not None:
            validated_action = player_intention.intentions[0]
            self.sub_application.logger.debug('Validated action :' + str(validated_action))
        previous_action = self.sub_application.environment.controlled_agent.controller.previous_action
        if previous_action is not None:
            if previous_action.type == self.game_class_.ACTION_INTENTION:
                desired_action = previous_action.params['action']
                self.sub_application.logger.debug('Desired action :' + str(desired_action))
        selected_squares = self.get_spotlight_squares_from_state(UIBoardSquare.STATE_SELECTED)
        self.sub_application.logger.debug('selected_squares :' + str(selected_squares))

        select, remove, errors = SMUIBoard.calculate_state_square_for_moving(validated_action,
                                                                             desired_action,
                                                                             selected_squares)
        self.sub_application.logger.debug('select :' + str(select))
        self.sub_application.logger.debug('remove :' + str(remove))
        self.sub_application.logger.debug('errors :' + str(errors))

        for s in select:
            self.add_spotlight_square_state(s, UIBoardSquare.STATE_SELECTED)
        for s in remove:
            self.remove_spotlight_square_state(s, UIBoardSquare.STATE_SELECTED)
        for s in errors:
            self.add_spotlight_square_state(s, UIBoardSquare.STATE_ERROR)

        # handling doors
        self._update_doors_with_observation(validated_action, desired_action, state)

        step = state.objects['State.TourJeu'].object_state.characteristics['Etape'].value
        self.pawn_step.set_relative_position((self.background_image.get_relative_rect()[2] * step_upleft[step - 1],
                                              int(self.background_image.get_relative_rect()[3] * 0.0454)))

    def update(self, time_delta: float):
        """
        Update the board before display

        :param time_delta: time passed since the last call
        """
        # self._update_debug()
        super(SMUIBoard, self).update(time_delta)

        # remove door state ERROR after fade out is finised
        for d in self.door_tokens.values():
            if d.alpha == 0:
                d.state = None
