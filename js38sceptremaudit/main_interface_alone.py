import os
import sys

from numpy import ndarray  # not usefull but needed by pyinstaller (windows) to be imported

from pymasep.app import App
from js38sceptremaudit import SMInterface


def main():
    """
    Main application launching only the interface. Search the engine on LAN
    """
    root_path = getattr(sys, '_MEIPASS', os.path.abspath(os.path.join(os.path.dirname(__file__))))
    application = App(SMInterface, root_path=root_path, config_path='data/config',
                      launch=App.LAUNCH_INTERFACE)
    application.run()


if __name__ == "__main__":
    main()
