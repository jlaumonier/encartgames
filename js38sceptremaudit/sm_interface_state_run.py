import pygame
from pygame_gui.windows import UIConsoleWindow

from pymasep.interface import InterfaceState

import js38sceptremaudit.sm_interface_state_init as state_init
from js38sceptremaudit.game import SMGame, sm_map
from js38sceptremaudit.sm_ui_board import SMUIBoard
from js38sceptremaudit.sm_sheet import SMSheet
from js38sceptremaudit.sm_action_player import SMActionPlayer
from js38sceptremaudit.sm_user_event import SM_ACTION_CHOICE


class SMInterfaceStateRun(InterfaceState):
    """
    This class implements the Run State of the interface. Used to run the game

    :param sub_application: the sub application where the state is defined
    """

    def __init__(self, sub_application):
        super().__init__(sub_application, sub_application.resources['background'].get_size())
        self.ui_map = None
        self.sheet = None
        self.game_log = None
        self.action_player = None

    def init(self):
        """
        Init the run state : init Board, Sheet, ActionPlayer and Log interface elements
        """
        super().init()
        board_size = (int(self.sub_application.virtual_screen.get_width() * 3/4),
                      self.sub_application.virtual_screen.get_height())
        self.ui_map = SMUIBoard(relative_rect=pygame.Rect((0, 0),
                                                          board_size),
                                manager=self.sub_application.ui_manager,
                                container=self.ui_container,
                                sub_application=self.sub_application,
                                image_surface=self.sub_application.resources['background'],
                                game_data=sm_map,
                                game_class_=SMGame,
                                origin_coord_map=(self.sub_application.resources['background'].get_size()[0]*0.154,
                                                  self.sub_application.resources['background'].get_size()[1]*0.128),
                                square_size=int(self.sub_application.resources['background'].get_size()[0]*0.0264534),
                                player_name='')
        self.sheet = SMSheet(rect=pygame.Rect((board_size[0], 0),
                                              (self.sub_application.virtual_screen.get_width()-self.ui_map.get_size()[0],
                                               self.sub_application.virtual_screen.get_height()*0.4)),
                             manager=self.sub_application.ui_manager,
                             sub_application=self.sub_application,
                             agent_name='')

        self.action_player = SMActionPlayer(
            rect=pygame.Rect((board_size[0],
                              self.sub_application.virtual_screen.get_height()/2.5),
                             (self.sub_application.virtual_screen.get_width()-self.ui_map.get_size()[0],
                              self.sub_application.virtual_screen.get_height()*0.2)),
            manager=self.sub_application.ui_manager,
            sub_application=self.sub_application)

        self.game_log = UIConsoleWindow(pygame.Rect((board_size[0],
                                                     self.sub_application.virtual_screen.get_height()*0.6),
                                                    (self.sub_application.virtual_screen.get_width()-self.ui_map.get_size()[0],
                                                     self.sub_application.virtual_screen.get_height()*0.35)),
                                        manager=self.sub_application.ui_manager,
                                        window_title='Evènements du jeu',
                                        object_id='#gamelog',
                                        visible=True)

    def handle_event(self, event):
        """
        Handle events : Action choice
        :param event: the event (SM_ACTION_CHOICE)
        """
        if event.type == SM_ACTION_CHOICE:
            if event.action == 'Déplacement':
                self.ui_map.set_mode(SMUIBoard.SELECT_SQUARE_MODE)
            if event.action == 'Ouvrir':
                self.ui_map.set_mode(SMUIBoard.SELECT_DOOR_MODE)

    def render(self, time_delta):
        """
        Render the state, do nothing special
        """
        pass

    def update(self):
        """ Update elements to render them. Go to 'init' state if environment state is 'init'"""
        super().update()
        current_state = self.sub_application.environment.current_state
        if current_state is not None:
            game_phase = self.sub_application.environment.game.get_system_value(current_state, 'GamePhase')
            if game_phase == 'init':
                self.sub_application.push_state(self.sub_application.app_states[state_init.SMInterfaceStateInit.__name__])

    def on_receive_observation(self):
        """ Executed on receive observation. Update game logs"""
        self.ui_map.player_name = self.sub_application.environment.controlled_agent.get_fullname()
        self.sheet.agent_name = self.sub_application.environment.controlled_agent.get_fullname()
        if 'game_log' in self.sub_application.environment.current_additional_info:
            for description in self.sub_application.environment.current_additional_info['game_log']:
                self.game_log.add_output_line_to_log(str(description))
