from pymasep.app import App
from js38sceptremaudit import SMInterface


def main():
    """ Main application launching the server only """
    application = App(SMInterface, config_path='data/config', launch=App.LAUNCH_SERVER_STANDALONE)
    application.run()


if __name__ == "__main__":
    main()
