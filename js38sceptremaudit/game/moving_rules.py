import pandas as pd

from pymasep.dices import roll_dices


class MovingRules:
    """
    Rules for moving
    """

    """ Table that describes the results of a fall """
    falling_table = pd.DataFrame([[1, 1, 6, -1, '', 0],
                                  [2, 1, 1, 0, 'Chute sur la 1ere case', 1],
                                  [2, 2, 2, 1, 'Chute sur la 2e case', 2],
                                  [2, 3, 6, -1, 'Pas de chute', 0],
                                  [3, 1, 1, 0, 'Chute sur la 1ere case', 2],
                                  [3, 2, 2, 1, 'Chute sur la 2e case', 3],
                                  [3, 3, 3, 2, 'Chute sur la 3e case', 4],
                                  [3, 4, 6, -1, 'Pas de chute', 0],
                                  [4, 1, 1, 0, 'Chute sur la 1ere case', 4],
                                  [4, 2, 2, 1, 'Chute sur la 2e case', 5],
                                  [4, 3, 3, 2, 'Chute sur la 3e case', 6],
                                  [4, 4, 4, 3, 'Chute sur la 4e case', 7],
                                  [4, 5, 6, -1, 'Pas de chute', 0],
                                  ],
                                 columns=['Cases', 'Dé départ', 'Dé fin', 'Chute sur', 'Chute desc', 'Dégats'])

    @staticmethod
    def move(positions, random_seed=None):
        """
        Move a movable token following a path

        :param positions: list of positions [x, y]. Positions must have been validated before.
        :param random_seed: random seed for testing only
        :return: final position, lost energy and list of string as a description of what happened
        """
        result_additional_information = []
        total, _ = roll_dices('1D6', random_seed)
        row = MovingRules.falling_table[(MovingRules.falling_table['Cases'] == len(positions)) &
                                        (total >= MovingRules.falling_table['Dé départ']) &
                                        (total <= MovingRules.falling_table['Dé fin'])]
        desc_falling = row['Chute desc'].iloc[0]
        lost_energy = int(row['Dégats'].iloc[0])
        if desc_falling != '':
            result_additional_information.append('Résultat du dé : ' + str(total))
            result_additional_information.append(desc_falling)
            if lost_energy != 0:
                result_additional_information.append('Perte de ' + str(lost_energy) + " point(s) d'énergie")
            else:
                result_additional_information.append("Pas de perte d'énergie")

        idx_pos_dest = row['Chute sur'].iloc[0]
        return positions[idx_pos_dest], lost_energy, result_additional_information

    @staticmethod
    def update_graph_with_door_status(graph_game_data, doors_objects):
        """
        update the graph of the possible path according to the state of the doors.

        :param graph_game_data: initial paths
        :param doors_objects: all doors used to update the paths
        :return: a new graph with paths updated
        """
        result = graph_game_data.copy()
        for door in doors_objects:
            if door.object_state.characteristics['status'].value == 'closed':
                node1 = tuple(door.object_state.characteristics['Pos1'].value)
                node2 = tuple(door.object_state.characteristics['Pos2'].value)
                result.remove_edge(node1, node2)
                result.remove_edge(node2, node1)
        return result

    @staticmethod
    def update_graph_with_tokens(graph_game_data, tokens_objects, agent_moving):
        """
        update the graph of the possible path according to the position of the tokens (exception the moving agent)

        :param graph_game_data: initial paths
        :param tokens_objects: all tokens used to update the paths
        :param agent_moving: the agent that are moving
        :return: a new graph with paths updated
        """
        result = graph_game_data.copy()
        for token in tokens_objects:
            if token != agent_moving:
                print(token.name)
                node = tuple(token.object_state.characteristics['Pos'].value)
                neighbors = [n for n in result.neighbors(node)]
                for neighbr in neighbors:
                    if result.has_edge(node, neighbr):
                        result.remove_edge(node, neighbr)
                    if result.has_edge(neighbr, node):
                        result.remove_edge(neighbr, node)
        return result

