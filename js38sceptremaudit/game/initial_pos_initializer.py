from pymasep.common.initializer import Initializer


class InitialPosInitializer(Initializer):
    """
    Initializer used to initialize the initial position of an player by the engine

    An initializer is created from a dictionary. the format is the following:
    name: name of the initializer used as id
    value_type: for Characteristic, the type of the value (as string)
    subclass_initializer: name of sub initializer used to initialize sub BaseObject.
                          These initializers must already exist in the game

    :param game: the game where the initializer is created
    :param initializer_dict: the dictionary used to create the initializer
    """

    def apply(self, base_object):
        """
        Apply the initializer.

        :param base_object: the player to initialize
        """
        agent_name = base_object.parent.parent.name
        player_nb = int(agent_name[-1])
        base_object.value = self.game.initial_position[player_nb]
