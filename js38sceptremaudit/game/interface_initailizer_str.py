from pymasep.common.initializer import Initializer


class InterfaceInitializerStr(Initializer):
    """Useless ??"""

    def __init__(self, game, initializer_dict):
        super().__init__(game, initializer_dict)

    def apply(self, base_object):
        pass
