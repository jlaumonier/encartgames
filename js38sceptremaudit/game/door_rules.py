from pymasep.dices import roll_dices


class DoorRules:
    """
    Rules for handling doors
    """

    """ Door status : closed """
    STATUS_CLOSED = 'closed'
    """ Door status : opened """
    STATUS_OPENED = 'opened'
    """ Door status : broken """
    STATUS_BROKEN = 'broken'

    @staticmethod
    def open(door_state, strength, random_seed=None):
        """
        Rules to open a door

        :param door_state: current state of the door
        :param strength: strength of the player who tru to open
        :param random_seed: random seed for testing only
        :return: result door state, energy lost by the player,
                 and additional information (textual description of the result)
        """
        result_door_state = door_state
        result_lost_energy = 0
        result_additional_information = []
        total, _ = roll_dices('2D6', random_seed)
        result_additional_information.append('Résultat du dé : ' + str(total))
        if total <= strength:
            result_door_state = DoorRules.STATUS_BROKEN
            result_additional_information.append('Porte enfoncée')
        else:
            result_lost_energy = 2
            result_additional_information.append("Échec de l'ouverture de la porte")
            result_additional_information.append("Perte de 2 points d'énergie")
        return result_door_state, result_lost_energy, result_additional_information
