from typing import List, Optional, Tuple


class BuyRules(object):

    @staticmethod
    def buy_rules(existing_intention: List[str],
                  new_add_intention: List[str],
                  remove_existing_intention: List[str],
                  available_ressource: Optional[int] = None,
                  objects_costs: Optional[dict] = None) -> Tuple[List[str], int, int]:

        existing_intention = list(set(existing_intention).difference(remove_existing_intention))
        result = existing_intention
        remaining_ressources = available_ressource
        costs = None
        if available_ressource is not None and objects_costs is not None:
            sum_sell = sum([objects_costs[o] for o in remove_existing_intention])
            remaining_ressources = remaining_ressources + sum_sell

            sum_cost = sum([objects_costs[o] for o in new_add_intention])
            if remaining_ressources >= sum_cost:
                result = result + new_add_intention
                remaining_ressources = remaining_ressources - sum_cost
            costs = sum([objects_costs[o] for o in result])
        else:
            result = result + new_add_intention
        return result, remaining_ressources, costs
