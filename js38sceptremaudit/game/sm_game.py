from omegaconf import DictConfig
import pymasep as pm
from pymasep.common.math import GridTools
from pymasep.common.initializer import (InitializerFixed, InitializerObjectStateInterface, \
                                        InitializerObjectInterface, InitializerCharacInterface,
                                        InitializerCharacInterfaceShared,
                                        InitializerObject, InitializerObjectState, InitializerContainer,
                                        InitializerContainerInterface)
from pymasep.utils import rm_spc_acc

from js38sceptremaudit.game.initial_pos_initializer import InitialPosInitializer
from .game_data import sm_map
from .moving_rules import MovingRules
from .door_rules import DoorRules
from .buy_rules import BuyRules


class SMGame(pm.common.Game):
    """
    Represents the rules of the game Sceptre Maudit.

    :param cfg: DictConfig containing the configuration. See :ref:`config` for more information.
    """
    """ Action : go to north : should not be used ?"""
    ACTION_NORTH = 1
    """ Action : go to south : should not be used ?"""
    ACTION_SOUTH = 2
    """ Action : go to east : should not be used ?"""
    ACTION_EAST = 3
    """ Action : go to west : should not be used ?"""
    ACTION_WEST = 4
    """ Action move a player"""
    ACTION_MOVE = 5
    """ Action open a door """
    ACTION_OPEN = 6
    """ Action buy an object """
    ACTION_BUY = 7
    """ ACTION sell an object """
    ACTION_SELL = 8

    PIONS = [('Homme jaune', 'pion_pj_m_jaune'),
             ('Homme bleu', 'pion_pj_m_bleu'),
             ('Homme vert', 'pion_pj_m_vert'),
             ('Femme bleu', 'pion_pj_f_bleu'),
             ('Femme orange', 'pion_pj_f_orange'),
             ('Femme rose', 'pion_pj_f_rose')]

    ARMES = [('Épée courte', 20, '1D6+2'),
             ('Épée longue', 40, '1D6+4'),
             ('Épée à deux mains', 60, '1D6+6')]

    def __init__(self, cfg: DictConfig = None):
        super().__init__(cfg=cfg)

        self.max_step = 40

        self.graph_game_data = GridTools.create_graph(sm_map)
        # Adding initial position
        initial_move_pos = [(13, 19), (14, 19)]
        self.initial_position = [(10, 20), (11, 20), (12, 20), (13, 20), (14, 20), (15, 20)]
        for p in self.initial_position:
            for p_m in initial_move_pos:
                # direction initial_position to initial_mov_pos
                self.graph_game_data.add_edge(p, p_m)

        self.objects_costs = {a[0]: a[1] for a in self.ARMES}

    def define_coord_methods(self):
        """
        Define the dictionary of coordination methods according to the game phase
        """
        super().define_coord_methods()
        self.coord_method['init'] = SMGame.MULTIPLAYER_COORDINATION_FREE_FOR_ALL

    def add_weapon_initializer(self):
        arme_init = {}
        for w in self.ARMES:
            self.add_initializer(InitializerFixed(self, {'name': 'ArmeNomInitializer' + rm_spc_acc(w[0]),
                                                         'value_type': "str",
                                                         'value': w[0]}
                                                  ))

            dommage_initializer = InitializerFixed(self, {'name': 'DommageInitializer' + rm_spc_acc(w[0]),
                                                          'value_type': "str",
                                                          'value': w[2]})
            self.add_initializer(dommage_initializer)

            self.add_initializer(InitializerObjectState(self, {'name': 'ArmeOSInitializer' + rm_spc_acc(w[0]),
                                                               "subclass_initializer": {
                                                                   'dommages': 'DommageInitializer' + rm_spc_acc(w[0]),
                                                                   'Nom': 'ArmeNomInitializer' + rm_spc_acc(w[0])}}))

            initializer = InitializerObject(self, {'name': 'ArmeInitializer' + rm_spc_acc(w[0]),
                                                   'subclass_initializer': {
                                                       'objectstate': 'ArmeOSInitializer' + rm_spc_acc(w[0])}})
            self.add_initializer(initializer)
            arme_init[rm_spc_acc(w[0])] = initializer.name
        return arme_init

    def add_initializers(self):
        """
        This method adds initializers definitions to the game.
        """
        empyt_list_charac_init = InitializerFixed(self,
                                                  {'name': 'EmptyListInit', 'value_type': "list", 'value': []})
        self.add_initializer(empyt_list_charac_init)
        self.add_initializer(InitializerFixed(self, {'name': 'EmptyStr', 'value_type': "str", 'value': ''}))
        self.add_initializer(InitializerFixed(self, {'name': 'POInitializer', 'value_type': "int", 'value': 100}))

        str_charac_init = InitializerCharacInterface(self,
                                                     {'name': 'StrInterfaceInit',
                                                      'value_type': "str",
                                                      'value_mode': InitializerCharacInterface.VALUE_MODE_VALUE})
        self.add_initializer(str_charac_init)
        strength_charac_init = InitializerCharacInterface(self,
                                                          {'name': 'StrenghtInterfaceInit',
                                                           'value_type': "int",
                                                           'value_mode': InitializerCharacInterface.VALUE_MODE_DICE,
                                                           'param': '1D6+3'})
        self.add_initializer(strength_charac_init)
        token_interface_init = InitializerCharacInterfaceShared(self,
                                                                {'name': 'TokenInterfaceInit',
                                                                 'value_type': "str",
                                                                 'value_mode': InitializerCharacInterface.VALUE_MODE_CHOICE,
                                                                 'param': self.PIONS})
        self.add_initializer(token_interface_init)

        self.add_initializer(InitializerFixed(self, {'name': 'InterfaceEnergy', 'value_type': "int", 'value': 30}))
        self.add_initializer(InitialPosInitializer(self, {'name': 'InterfacePos', 'value_type': "tuple"}))
        self.add_initializer(InitializerFixed(self, {'name': 'InterfaceClosedDoor', 'value_type': "str",
                                                     'value': DoorRules.STATUS_CLOSED}))

        armes_inits = self.add_weapon_initializer()
        # self.add_initializer(
        #     InitializerContainer(self, {'name': 'ArmeContainerInitializer', 'obj_init': ('Arme', 'ArmeInitializer')}))

        obj_init_armes = {o[0]: {'cost': o[1],
                                 'template': 'Arme',
                                 'initializer': armes_inits[rm_spc_acc(o[0])]} for o in self.ARMES}
        self.add_initializer(
            InitializerContainerInterface(self, {'name': 'ArmeContainerInitializer',
                                                 'type': 'BUY',
                                                 'obj_init': obj_init_armes})
        )

        agent_charac_initializer = {'Pos': "InterfacePos",
                                    'Token': "TokenInterfaceInit",
                                    'Nom': "StrInterfaceInit",
                                    'Energie': 'InterfaceEnergy',
                                    'Force': 'StrenghtInterfaceInit',
                                    'PO': 'POInitializer'}

        # # Test only. to avoid the player initialization by interface
        # self.add_initializer(InitializerFixed(self, {'name': 'TestNameInit', 'value_type': "str", 'value': 'test'}))
        # self.add_initializer(InitializerFixed(self, {'name': 'TestForceInit', 'value_type': "int", 'value': 8}))
        # self.add_initializer(
        #     InitializerFixed(self, {'name': 'TestTokenInit', 'value_type': "str", 'value': 'pion_pj_m_jaune'}))
        # agent_charac_initializer['Token'] = 'TestTokenInit'
        # agent_charac_initializer['Nom'] = 'TestNameInit'
        # agent_charac_initializer['Force'] = 'TestForceInit'
        # # end

        self.add_initializer(InitializerObjectStateInterface(self, {'name': 'InterfaceAgentObjectState',
                                                                    "subclass_initializer": agent_charac_initializer}))
        self.add_initializer(InitializerObjectInterface(self, {'name': 'PlayerInitializer',
                                                               "subclass_initializer": {
                                                                   'objectstate': "InterfaceAgentObjectState",
                                                                   'containers': [
                                                                       ("Armes", "ArmeContainerInitializer")]}}))

    def next_state(self, current_state, actions):
        """
        Calculates the new state according to the current state and the action of the agent.

        :param current_state: the current state
        :param actions: action of the agents
        :return: the new state
        """
        result = current_state
        additional_info = dict()
        additional_info['game_log'] = []

        coord_method = self.coord_method[self.get_system_value(result, 'GamePhase') ]
        agent_acts = []
        if coord_method == self.MULTIPLAYER_COORDINATION_TURN:
            agent_acts.append(self.get_system_value(current_state, 'AgentOrder').head)
        else:
            agent_acts.extend([a.get_fullname() for a in result.agents])

        for active_player_name in agent_acts:
            ag = result.objects[active_player_name]
            if active_player_name in actions:
                action = actions[active_player_name]
                pos_player = current_state.objects[active_player_name].object_state.characteristics['Pos'].value
                res_pos_charac = result.objects[active_player_name].object_state.characteristics['Pos']
                if action.type == SMGame.ACTION_EAST and GridTools.is_connected_by_direction(sm_map, pos_player, 'E'):
                    res_pos_charac.value = (pos_player[0] + 1, pos_player[1])
                    additional_info['state_changed'] = True
                if action.type == SMGame.ACTION_WEST and GridTools.is_connected_by_direction(sm_map, pos_player, 'W'):
                    res_pos_charac.value = (pos_player[0] - 1, pos_player[1])
                    additional_info['state_changed'] = True
                if action.type == SMGame.ACTION_SOUTH and GridTools.is_connected_by_direction(sm_map, pos_player, 'S'):
                    res_pos_charac.value = (pos_player[0], pos_player[1] + 1)
                    additional_info['state_changed'] = True
                if action.type == SMGame.ACTION_NORTH and GridTools.is_connected_by_direction(sm_map, pos_player, 'N'):
                    res_pos_charac.value = (pos_player[0], pos_player[1] - 1)
                    additional_info['state_changed'] = True

                if action.type == SMGame.ACTION_INIT_OBJECT:
                    additional_info['state_changed'] = True
                    for act_name, param in action.params.items():
                        if act_name != 'buy':
                            if result.objects[active_player_name].object_state.characteristics[act_name].state == 'init':
                                result.objects[active_player_name].object_state.characteristics[
                                    act_name].value = param.value
                        else:
                            container = result.objects[active_player_name].containers['Armes']
                            object_to_buy = None
                            if param is not None:
                                object_to_buy = param.params['object']
                                ag.object_state.characteristics['PO'].value -= param.params['costs']
                            container.initializer.apply_with_params(container, object_to_buy)

                if action.type == SMGame.ACTION_INTENTION:
                    action_intention = action.params['action']

                    if action_intention.type == SMGame.ACTION_MOVE:
                        additional_info['state_changed'] = True
                        list_intention_pos = []
                        if ag.intention is not None and ag.intention.intentions[0].type == SMGame.ACTION_MOVE:
                            list_intention_pos = ag.intention.intentions[0].params['pos']
                        doors = [result.objects[o] for o in result.objects if result.objects[o].nature == 'Door']
                        tokens = [result.objects[o] for o in result.objects if result.objects[o].nature == 'Player']
                        graph_game_data_with_door = MovingRules.update_graph_with_door_status(self.graph_game_data, doors)
                        graph_game_data_final = MovingRules.update_graph_with_tokens(graph_game_data_with_door,
                                                                                     tokens,
                                                                                     ag)
                        list_pos = GridTools.adapt_path_from_selected(graph_game_data_final, pos_player,
                                                                      list_intention_pos, action_intention.params['pos'][0])
                        if list_pos and len(list_pos) <= 4:
                            if ag.intention is None or ag.intention.intentions[0].type != SMGame.ACTION_MOVE:
                                ag.intention = current_state.environment.create_object(name="intention",
                                                                                       template=self.templates[
                                                                                           'EmptyIntentionTemplate'])
                                ag.intention.add_intention(action_intention)
                            else:
                                ag.intention.intentions[0].params['pos'] = list_pos
                        if not list_pos:
                            ag.intention = None

                    if action_intention.type == SMGame.ACTION_OPEN:
                        additional_info['state_changed'] = True
                        existing_intention_door = None
                        if ag.intention is not None and ag.intention.intentions[0].type == SMGame.ACTION_OPEN:
                            existing_intention_door = ag.intention.intentions[0].params['door_name']
                        door_name = action_intention.params['door_name']
                        door = result.objects[door_name]
                        door_pos = (door.object_state.characteristics['Pos1'].value,
                                    door.object_state.characteristics['Pos2'].value)
                        if pos_player in door_pos \
                                and door.object_state.characteristics['status'].value == DoorRules.STATUS_CLOSED \
                                and existing_intention_door != door_name:
                            ag.intention = current_state.environment.create_object(name="intention",
                                                                                   template=self.templates[
                                                                                       'EmptyIntentionTemplate'])
                            ag.intention.add_intention(action_intention)
                        else:
                            if existing_intention_door == door_name:
                                ag.intention = None

                    if action_intention.type == SMGame.ACTION_BUY or action_intention.type == SMGame.ACTION_SELL:
                        additional_info['state_changed'] = True
                        object_agent_intention = []
                        object_intention_to_buy = []
                        object_intention_not_to_buy = []
                        if action_intention.type == SMGame.ACTION_BUY:
                            object_intention_to_buy = action_intention.params['object']
                        if action_intention.type == SMGame.ACTION_SELL:
                            object_intention_not_to_buy = action_intention.params['object']
                        old_cost = 0
                        if ag.intention is None:
                            ag.intention = current_state.environment.create_object(name="intention",
                                                                                   template=self.templates[
                                                                                       'EmptyIntentionTemplate'])
                        else:
                            object_agent_intention = ag.intention.intentions[0].params['object']
                            old_cost = ag.intention.intentions[0].params['costs']
                        available_ressource = ag.object_state.characteristics['PO'].value - old_cost
                        new_objects_in_intention, remaining_ressource, costs = BuyRules.buy_rules(object_agent_intention,
                                                                                                  object_intention_to_buy,
                                                                                                  object_intention_not_to_buy,
                                                                                                  available_ressource,
                                                                                                  self.objects_costs)
                        buy_intention = current_state.environment.create_action(SMGame.ACTION_BUY)
                        buy_intention.params['object'] = new_objects_in_intention
                        buy_intention.params['remaining_ressource'] = remaining_ressource  # ?
                        buy_intention.params['costs'] = costs
                        if len(ag.intention.intentions) == 0:
                            ag.intention.add_intention(buy_intention)
                        else:
                            ag.intention.intentions[0] = buy_intention

                if action.type == SMGame.ACTION_MOVE:
                    additional_info['state_changed'] = True
                    dest_pos, lost_energy, description = MovingRules.move(action.params['pos'])
                    result.objects[active_player_name].object_state.characteristics['Pos'].value = dest_pos
                    result.objects[active_player_name].object_state.characteristics['Energie'].value -= lost_energy
                    result.objects[active_player_name].intention = None
                    additional_info['game_log'].extend(description)
                    additional_info['game_log'].append(str(ag.name) + ' - Déplacement vers ' + str(dest_pos))

                if action.type == SMGame.ACTION_OPEN:
                    additional_info['state_changed'] = True
                    door_name = action.params['door_name']
                    door = result.objects[door_name]
                    strength = ag.object_state.characteristics['Force'].value
                    state_door, lost_energy, description = DoorRules.open(door.object_state.characteristics['status'].value,
                                                                          strength)
                    result.objects[door_name].object_state.characteristics['status'].value = state_door
                    result.objects[active_player_name].object_state.characteristics['Energie'].value -= lost_energy
                    result.objects[active_player_name].intention = None
                    additional_info['game_log'].extend(description)

                # increase step only if all players have taken an action
                action_for_inc_step = [SMGame.ACTION_MOVE, SMGame.ACTION_OPEN, SMGame.ACTION_INIT_OBJECT]
                if action.type in action_for_inc_step and action.type is not None:
                    agent_order = result.objects['State.system'].object_state.characteristics['AgentOrder'].value
                    agent_order.move_head_next()
                    next_player = agent_order.head
                    first_player = agent_order.data[0]
                    if next_player == first_player and action.type != SMGame.ACTION_INIT_OBJECT:
                        result.objects['State.TourJeu'].object_state.characteristics['Etape'].value += 1
                        additional_info['game_log'].append('- Nouveau tour de jeu ---------------------')
                    additional_info['game_log'].append('--------------------------------------------')

        if result.state == 'init':
            self.set_system_value(result, 'GamePhase', 'init')
        else:
            self.set_system_value(result, 'GamePhase', 'play')

        return result, additional_info

    def is_final(self, state):
        """
        Is the state final ?

        :param state: the state
        :return: True if final, False if not
        """
        return state.objects['State.TourJeu'].object_state.characteristics['Etape'].value == self.max_step
