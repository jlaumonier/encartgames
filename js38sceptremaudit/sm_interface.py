import os
import pygame
from typing import Optional
from omegaconf import DictConfig

import pymasep as pm
from pymasep.application import ConnectedSubAppInfo

from .sm_player_controller import SMPlayerController
from .sm_interface_state_init import SMInterfaceStateInit
from .sm_interface_state_run import SMInterfaceStateRun
from js38sceptremaudit.game import SMGame
from js38sceptremaudit.game import DoorRules


class SMInterface(pm.interface.Interface):
    """
        Sub application that display an Sceptre Maudit environment and interact with user

        :param app: main application
        :param received_q_id: id of the queue used by the sub app to receive messages.
        :param cfg: configuration of the interface
        :param game_class_: class of the game
        :param interface_role: Role of the interface. see @ConnectedSubAppInfo.
        :param remote_engine_host: host of the remote engine. May be None if interface is in the same process of engine
        :param remote_engine_port: port of the remote engine. May be None if interface is in the same process of engine
        :param search_remote: True if the interface have to search the server on LAN. Default False
        """
    DOOR_TOKENS = {DoorRules.STATUS_CLOSED: 'closed_door',
                   DoorRules.STATUS_OPENED: 'opened_door',
                   DoorRules.STATUS_BROKEN: 'broken_door'}

    def __init__(self, app, received_q_id: int, cfg: DictConfig = None,
                 remote_engine_host: Optional[str] = None,
                 remote_engine_port: Optional[int] = None,
                 search_remote: bool = False):
        super().__init__(app, received_q_id, cfg, SMGame, ConnectedSubAppInfo.ROLE_ACTOR,
                         remote_engine_host, remote_engine_port, search_remote)
        self.resources['error'] = pygame.image.load(os.path.join(self.root_path, 'data/images/error.png'))
        self.resources['background'] = pygame.image.load(os.path.join(self.root_path, 'data/images/background.png'))
        self.resources['pawn_step'] = pygame.image.load(os.path.join(self.root_path, 'data/images/pion_etape.png'))

        for (desc_p, value_p) in SMGame.PIONS:
            self.resources[value_p] = pygame.image.load(os.path.join(self.root_path, 'data/images/' + value_p + '.png'))

        self.resources[self.DOOR_TOKENS[DoorRules.STATUS_CLOSED]] = pygame.image.load(os.path.join(self.root_path,
                                                                                                   'data/images/porte_fermee.png'))
        self.resources[self.DOOR_TOKENS[DoorRules.STATUS_OPENED]] = pygame.image.load(os.path.join(self.root_path,
                                                                                                   'data/images/porte_ouverte.png'))
        self.resources[self.DOOR_TOKENS[DoorRules.STATUS_BROKEN]] = pygame.image.load(os.path.join(self.root_path,
                                                                                                   'data/images/porte_detruite.png'))
        self.virtual_screen_size = (1368, 768)
        self.controller_interface_class_ = SMPlayerController
        self.ui_theme_file = 'data/pygame_ui_theme.json'

        # add possible states
        self.app_states[SMInterfaceStateInit.__name__] = SMInterfaceStateInit(self)
        self.app_states[SMInterfaceStateRun.__name__] = SMInterfaceStateRun(self)

    def init_game(self):
        """
        Initialize game : Connect to engine, define the game and register self as a new interface. Start the Run state
        """
        super().init_game()
        # first app state is init
        self.push_state(self.app_states[SMInterfaceStateRun.__name__])
