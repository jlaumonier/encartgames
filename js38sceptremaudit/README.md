# Sceptre Maudit v0.2

Ce jeu est basé sur le jeu en encart "Le Sceptre maudit" du Jeux et Stratégie numéro 38. 

Les règles et les images sont sous copyright Gildas Sagot et Jeux et Stratégie (1996). Les images ont été créées par Claude Lacroix. Merci à eux !

L'implémentation en python est sous licence CC-BY-NC-SA 4.0.

## Règles implémentées

* Création : Nom, Force, Pion, Achat d'armes
* Déplacement
* Ouverture (destruction) de porte.  

## Exemple 

Jouer seul :
![sceptremaudit-v0.2-solo.gif](doc%2Fimages%2Fsceptremaudit-v0.2-solo.gif)

Jouer à plusieurs

![sceptremaudit-v0.2-2players.gif](doc%2Fimages%2Fsceptremaudit-v0.2-2players.gif)

## Manuel

Lancement du jeu :

(Linux X86_64)
* Décompressez sceptremaudit-v0.1-linux.tar.gz
* lancer ./js38sceptremaudit depuis le répertoire js38sceptremaudit/ pour lancer le moteur et l'interface
* lancer ./js38sceptremauditinterface depuis le répertoire js38sceptremauditinterface/ pour lancer l'interface seule. Le moteur doit avoir été lancé sur l'ordinateur local ou le réseau local.

(Windows X86_64)
* Décompressez sceptremaudit-v0.1-windows.zip
* lancer js38sceptremaudit.exe depuis le répertoire js38sceptremaudit/ pour lancer le moteur et l'interface
* lancer js38sceptremauditinterface.exe depuis le répertoire js38sceptremauditinterface/ pour lancer l'interface seule. Le moteur doit avoir été lancé sur l'ordinateur local ou le réseau local.

En python à partir du code, il existe trois manières de lancer le jeu :
* `main.py` : lance le moteur et l'interface
* `main_server_alone.py` : lance seulement le moteur. Les interfaces peuvent se connecter par `main_interface_alone.py`
* `main_interface_alone.py` : lancer seulement l'interface qui peut se connecter sur le moteur si celui-ci a été lancé par l'un des deux scripts précédents.

### Création du personnage

Une fenêtre de création de personnages s'affiche. Entrez le nom du personnage, sa force (ou cliquez sur le bouton pour lancer le dé), et, éventuellement, changez son pion. Avec la liste en dessous, vous pouvez acheter une arme en l'ajoutant à la liste de droite. Puis, cliquez sur valider. 

### Choix d'action

Le choix de l'action se fait à partir de la sous-fenêtre de droite en dessous de la feuille de personnage. Deux actions sont pour l'instant possibles : Déplacement et ouverture.

### Déplacement du personnage

Le déplacement se fait en cliquant (bouton gauche) sur les cases (le chemin) où on souhaite passer. Si le personnage peut se déplacer sur cette case, la case restera en vert sinon elle s'affichera un instant en rouge. Une fois le chemin dessiné, on le valide avec le bouton droit. Selon les règles, le personnage peut se déplacer ou non sur toutes les cases et/ou perdre des points d'énergie. Il ne peut pas se déplacer sur une case où se trouve un autre personnage.

### Ouverture de porte

Pour ouvrir une porte, il faut être à côté. On sélectionne la porte avec le bouton gauche, puis on valide avec le bouton droit. Selon les règles, la porte peut s'ouvrir ou non.

### Statistiques

Ctrl+d affiche une petite fenêtre de statistique (position de la souris, etc.), les FPS sont affichés en bas à gauche.

