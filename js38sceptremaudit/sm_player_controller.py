from pygame.locals import *
from pymasep.common import State, Agent, Environment
from pymasep.interface import InterfaceController, SQUARE_SELECTED_EVENT, SQUARE_VALIDATION_EVENT
from js38sceptremaudit.game import SMGame
from js38sceptremaudit.sm_ui_board import SM_ACTION_DOOR_SELECTED, SM_ACTION_DOOR_VALIDATED


class SMPlayerController(InterfaceController):
    """
    Interface controller that transform interface event to player action/intentions

    :param environment: the interface environment
    """

    """ the next action is next_action """
    COMMAND_NEXT_ACTION = 1
    """ the next action is the intention of the agent """
    COMMAND_INTENTION = 2

    def __init__(self, environment: Environment):
        super().__init__(environment)
        self.environment = environment
        # action chosen by event
        self.next_action = None
        self.previous_action = None
        # What should be the next action
        self.command = SMPlayerController.COMMAND_NEXT_ACTION

    def action_choice(self, observation: State, agent: Agent):
        """
        Choose the action.

        :param observation the observation used to choose the action
        :param agent the agent who choose the action
        :return the action chosen for the agent.
        """
        result = None
        self.previous_action = self.next_action
        if self.command == SMPlayerController.COMMAND_NEXT_ACTION:
            result = self.next_action
        if self.command == SMPlayerController.COMMAND_INTENTION:
            result = agent.intention.intentions[0]
        self.next_action = None
        self.command = SMPlayerController.COMMAND_NEXT_ACTION
        return result

    def select_action(self, event):
        """
        Selection action according to the interface event
        :param event: the event (KEYDOWN, SQUARE_SELECTED_EVENT, SQUARE_VALIDATION_EVENT, SM_ACTION_DOOR_SELECTED,
        SM_ACTION_DOOR_VALIDATED
        :return: true if the event has been processed
        """
        self.command = SMPlayerController.COMMAND_NEXT_ACTION
        event_processed = False
        self.next_action = None
        if event.type == SQUARE_SELECTED_EVENT:
            self.next_action = self.environment.create_action(SMGame.ACTION_INTENTION)
            action_intention = self.environment.create_action(SMGame.ACTION_MOVE)
            action_intention.params['pos'] = [event.square_pos]
            self.next_action.params['action'] = action_intention
            event_processed = True
        if event.type == SQUARE_VALIDATION_EVENT:
            self.command = SMPlayerController.COMMAND_INTENTION
            event_processed = True
        if event.type == SM_ACTION_DOOR_SELECTED:
            self.next_action = self.environment.create_action(SMGame.ACTION_INTENTION)
            action_intention = self.environment.create_action(SMGame.ACTION_OPEN)
            action_intention.params['door_name'] = event.door_name
            self.next_action.params['action'] = action_intention
            event_processed = True
        if event.type == SM_ACTION_DOOR_VALIDATED:
            self.command = SMPlayerController.COMMAND_INTENTION
            event_processed = True
        return event_processed

