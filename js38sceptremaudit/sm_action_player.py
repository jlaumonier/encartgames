from pygame import Rect
import pygame.event
from pygame_gui.elements import UIWindow
from pygame_gui.elements import UILabel, UISelectionList
from pygame_gui import UI_SELECTION_LIST_NEW_SELECTION

from js38sceptremaudit.sm_user_event import SM_ACTION_CHOICE


class SMActionPlayer(UIWindow):
    """
    Inferface element that display information about the player (Active player, choice of action)

    :param rect: position and size of the element
    :param manager: Manager (from pygame_gui)
    :param sub_application: the interface where the element is defined
    """

    def __init__(self, rect, manager, sub_application):
        super().__init__(rect, manager,
                         window_display_title='Actions du joueur',
                         object_id='#playeraction',
                         resizable=False
                         )
        self.sub_application = sub_application
        font_info = self.ui_manager.get_theme().get_font_info(self.object_ids)
        margin = int(font_info['size']) / 2

        self.active_player_label = UILabel(Rect((margin, margin),
                                                (self.get_relative_rect()[2], int(font_info['size']) * 1.5)),
                                           text='Joueur Actif : ',
                                           manager=self.ui_manager,
                                           container=self,
                                           object_id='#playeraction')
        self.possible_action = UISelectionList(Rect((margin,
                                                     self.active_player_label.get_relative_rect()[1] + int(
                                                         int(font_info['size']) * 1.5)),
                                                    (self.active_player_label.get_relative_rect()[2] - 5 * margin,
                                                     (int(font_info['size']) * 1.5) * 3 + 4 * margin)),
                                               item_list=['Déplacement', 'Ouvrir'],
                                               default_selection='Déplacement',
                                               manager=self.ui_manager,
                                               container=self,
                                               object_id='#playeraction')

        self.change_layer(50)

    def process_event(self, event: pygame.event.Event) -> bool:
        """
        Process an event on this element : selection of the action
        :param event: event to process.
        :return: True is the event have been processed
        """
        event_processed = False
        if event.type == UI_SELECTION_LIST_NEW_SELECTION:
            if event.ui_element == self.possible_action:
                evt_square_validation = pygame.event.Event(SM_ACTION_CHOICE, action=event.text)
                pygame.event.post(evt_square_validation)
                event_processed = True
        return event_processed

    def update(self, time_delta: float):
        """
        Update element to display
        :param time_delta: time passed since the last call
        """
        super().update(time_delta)
        cs = self.sub_application.environment.current_state
        joueur_actif = cs.objects['State.system'].object_state.characteristics['AgentOrder'].value.head
        self.active_player_label.set_text('Joueur actif : ' + joueur_actif)
