import pygame

from pygame_gui.windows import UIMessageWindow

from pymasep.interface import UIInitObjectWindow
from pymasep.interface.interface_state import InterfaceState
import pymasep.interface.user_event as user_event
from js38sceptremaudit.sm_player_controller import SMPlayerController
from js38sceptremaudit.game import SMGame


class SMInterfaceStateInit(InterfaceState):
    """
    This class implements the Init State of the interface. Used to initialize players

    :param sub_application: the sub application where the state is defined
    """

    def __init__(self, sub_application):
        super().__init__(sub_application, sub_application.resources['background'].get_size())
        self.window_init_object = None
        self.window_wait = None

    def handle_event(self, event):
        """
        Handle the event of INIT_WINDOW_VALIDATION_EVENT
        :param event: the event to handle
        """
        if event.type == user_event.INIT_WINDOW_VALIDATION_EVENT:
            ac = self.sub_application.environment.create_action(SMGame.ACTION_INIT_OBJECT)
            game = self.sub_application.environment.game
            for charac_name, entry in self.window_init_object.entries.items():
                chara = self.sub_application.environment.create_object(name=charac_name,
                                                                       template=game.templates[charac_name])
                chara.value = chara.value_type(entry.get_text())
                ac.add_parameter(charac_name, chara)
                entry.set_text("")

            # get the buy intention of the agent
            ag = self.sub_application.environment.controlled_agent
            intention = ag.intention.intentions[0] if ag.intention is not None else None
            ac.add_parameter('buy', intention)

            self.sub_application.environment.controlled_agent.controller.command = SMPlayerController.COMMAND_NEXT_ACTION
            self.sub_application.environment.controlled_agent.controller.next_action = ac

        if event.type == user_event.MERCHANT_ADD_OBJECT_EVENT or event.type == user_event.MERCHANT_REMOVE_OBJECT_EVENT:
            ac = self.sub_application.environment.create_action(SMGame.ACTION_INTENTION)
            ac_intention = None
            if event.type == user_event.MERCHANT_ADD_OBJECT_EVENT:
                ac_intention = self.sub_application.environment.create_action(SMGame.ACTION_BUY)
            if event.type == user_event.MERCHANT_REMOVE_OBJECT_EVENT:
                ac_intention = self.sub_application.environment.create_action(SMGame.ACTION_SELL)
            ac_intention.params['object'] = event.obj
            ac.params['action'] = ac_intention
            self.sub_application.environment.controlled_agent.controller.command = SMPlayerController.COMMAND_NEXT_ACTION
            self.sub_application.environment.controlled_agent.controller.next_action = ac

    def update(self):
        """
        the update phase of the state. For interface, it corresponds to choose action, send action and receive
        observation. In this case, the init window is created if it does exist. The interface state is popped is the
        environment state is not 'init' anymore.
        """
        super().update()

        center_x = self.ui_container.rect.size[0] / 2
        game = self.sub_application.environment.game
        #active_player_name = game.get_system_value(self.sub_application.environment.current_state, 'AgentOrder').head
        ag = self.sub_application.environment.controlled_agent
        if self.sub_application.environment.controlled_agent.state == 'init':
                #and \
                #active_player_name == self.sub_application.environment.controlled_agent.get_fullname()):
            if self.window_wait is not None:
                self.window_wait.kill()
            if self.window_init_object is None:
                self.window_init_object = UIInitObjectWindow(
                    rect=pygame.Rect(
                        (center_x - self.ui_container.rect.size[0] * 0.3 / 2, self.ui_container.rect.size[1] * 0.2),
                        (self.ui_container.rect.size[0] * 0.5, self.ui_container.rect.size[1] * 0.75)),
                    ui_manager=self.sub_application.ui_manager,
                    sub_application=self.sub_application,
                    object_to_init_name=ag.get_fullname())

                self.window_init_object.change_layer(500)
        else:
            if self.window_init_object is not None:
                self.window_init_object.kill()
            if self.window_wait is None:
                self.window_wait = UIMessageWindow(pygame.Rect(
                    (center_x - self.ui_container.rect.size[0] * 0.3 / 2, self.ui_container.rect.size[1] * 0.3),
                    (self.ui_container.rect.size[0] * 0.5, self.ui_container.rect.size[1] / 2)),
                    "Veuillez attendre votre tour pour initialiser votre personnage "
                    "ou que les autres joueurs aient terminé.")
                self.window_wait.change_layer(500)
                self.window_wait.dismiss_button.hide()

        game_phase = self.sub_application.environment.game.get_system_value(self.sub_application.environment.current_state, 'GamePhase')
        if game_phase != 'init':
            self.sub_application.pop_state()

    def clean(self):
        """
        The state is destroyed. Clean all elements of the state (including ui_container)
        """
        if self.window_init_object is not None:
            self.window_init_object.kill()

        if self.window_wait is not None:
            self.window_wait.kill()

    def render(self, time_delta):
        """
        The render part : how to display the state. Nothing special in this state
        """
        pass
